variable "environment" {
  description = "Environment name used for naming resouces"
  type        = string
}

variable "domain" {
  description = "Base domain to use for HTTPS endpoints"
  type        = string
}
