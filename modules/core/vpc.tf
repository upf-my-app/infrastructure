module "vpc" {
  source                       = "terraform-aws-modules/vpc/aws"
  azs                          = data.aws_availability_zones.available.names
  cidr                         = "10.2.0.0/16"
  create_database_subnet_group = true
  database_subnets             = ["10.2.7.0/24", "10.2.8.0/24", "10.2.9.0/24"]
  enable_dns_hostnames         = true
  enable_nat_gateway           = false
  name                         = local.service_name
  private_subnets              = ["10.2.16.0/20", "10.2.32.0/19", "10.2.64.0/19"]
  public_subnets               = ["10.2.96.0/19", "10.2.128.0/19", "10.2.160.0/19"]
  single_nat_gateway           = true
  version                      = "3.13.0"
}

