locals {
  service_name       = "my-app-${var.environment}"
  environment_suffix = var.environment == "staging" ? "-staging" : ""
  service_domain     = "my-app${local.environment_suffix}.${var.domain}"
}

