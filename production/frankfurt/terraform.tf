terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region = "eu-central-1"
  default_tags {
    tags = {
      environment = "production"
      project     = "my-app"
    }
  }
}


